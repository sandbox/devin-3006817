<?php

function singer_drush_help($section) {
  switch ($section) {
    case 'drush:singer-json':
      return dt('Get Singer JSON catalog.');
  }
}

/**
 * Get singer catalog JSON.
 */
function drush_singer_json() {
  $json = singer_make_catalog();
  return json_encode($json);
}
